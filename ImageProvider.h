#ifndef IMAGEPROVIDER_H
#define IMAGEPROVIDER_H

#include <QQuickAsyncImageProvider>
#include <QQuickImageResponse>
#include <QObject>
#include <QStandardPaths>
#include <QDir>
#include <QTimer>
#include <QRandomGenerator>
#include <QThread>

class FileReadWorker : public QObject
{
	Q_OBJECT
	QString id;
	QSize requestedSize;

public:
	FileReadWorker (const QString &_id, const QSize &_requestedSize, QObject *parent = nullptr)
		: QObject (parent), id (_id), requestedSize (_requestedSize)
	{
	}
signals:
	void done (QImage data);
public slots:
	void doTheWork ()
	{
		qDebug() << "Image Requested :: " << id;
		QStringList slp = QStandardPaths::standardLocations (QStandardPaths::PicturesLocation);
		if (slp.isEmpty())
		{
			// Error
			return;
		}
		QStringList ImagesList = QDir (slp.at (0) + "/Wallpapers/").entryList (QDir::NoDotAndDotDot | QDir::Files);
		qDebug() << "Image Files in " << slp.at (0) + "/Wallpapers/"
				 << "List :" << ImagesList;
		QImage aImage;
		if (!aImage.load (slp.at (0) + "/Wallpapers/" + ImagesList.at (id.toInt())))
		{
			qDebug() << "Error getting Image";
		}
		emit done (aImage);
	}
};

static int SrespNo = 0;
class AsyncImageResponse : public QQuickImageResponse
{
	Q_OBJECT
	QThread nt;

	int respNo;

public:
	AsyncImageResponse (const QString &id, const QSize &requestedSize)
	{
		respNo = SrespNo++;
		nw = new FileReadWorker (id, requestedSize);
		nw->moveToThread (&nt);
		nt.start (QThread::LowPriority);
		connect (this, &AsyncImageResponse::doStuff, nw, &FileReadWorker::doTheWork);
		connect (nw, &FileReadWorker::done, this, &AsyncImageResponse::doneStuff);
		int timeToWait = QRandomGenerator::global()->generate() % 5000;
		QTimer::singleShot (timeToWait, this, [this] () { emit doStuff(); });
	}

	~AsyncImageResponse()
	{
		qDebug() << "Async Image Response exiting : " << respNo;
		if (!(nw == nullptr))
		{
			delete nw;
		}
		nt.quit();
		nt.wait();
	}

	void doneStuff (QImage image)
	{
		m_image = image;
		emit finished();
		delete nw;
		nw = nullptr;
	}

	QQuickTextureFactory *textureFactory () const override { return QQuickTextureFactory::textureFactoryForImage (m_image); }

	QImage m_image;

	FileReadWorker *nw;

signals:
	void doStuff ();
};

class ImageProvider : public QQuickAsyncImageProvider
{
public:
	ImageProvider (ImageType type = Image, Flags flags = Flags());

	QImage requestImage (const QString &id, QSize *size, const QSize &requestedSize) override;

	QQuickImageResponse *requestImageResponse (const QString &id, const QSize &requestedSize) override
	{
		AsyncImageResponse *response = new AsyncImageResponse (id, requestedSize);
		return response;
	}
};

#endif		  // IMAGEPROVIDER_H
