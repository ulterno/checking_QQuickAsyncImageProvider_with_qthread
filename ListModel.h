#ifndef LISTMODEL_H
#define LISTMODEL_H

#include <QAbstractListModel>

class ListModel : public QAbstractListModel
{
	Q_OBJECT

public:
	enum LM_ROLES
	{
		DisplayRole = Qt::UserRole + 1,
		ImageIdRole,
		ImageNameRole,
		ImageDescriptionRole
	};

	explicit ListModel (QObject *parent = nullptr);

	// Basic functionality:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;

	QVariant data (const QModelIndex &index, int role = Qt::DisplayRole) const override;

	QHash<int, QByteArray> roleNames () const override;

	QString getImageProviderRoot () const;

private:
	QString imageProviderRoot = "ImProv";
};

#endif // LISTMODEL_H
