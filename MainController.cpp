#include "MainController.h"

MainController::MainController (QObject *parent) : QObject{ parent }, lm (new ListModel (parent)) { qDebug() << "MainController constructor"; }

ListModel *MainController::getLm() const
{
	qDebug() << "MainController::getLm()";
	return lm;
}
