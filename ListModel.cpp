#include "ListModel.h"

ListModel::ListModel (QObject *parent) : QAbstractListModel (parent) { qDebug() << "ListModel constructor"; }

int ListModel::rowCount(const QModelIndex &parent) const
{
	//	qDebug() << "ListModel Row Count";
	// For list models only the root node (an invalid parent) should return the list's size. For all
	// other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
	if (parent.isValid())
	{
		return 0;
	}
	return 5;
}

QVariant ListModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	switch (role)
	{
	case DisplayRole:
		return QString (tr ("%1, %2").arg (index.column()).arg (index.row()));
	case ImageIdRole:
		return QString ("image://" + imageProviderRoot + "/" + QString::number (index.row()));
	default:
		break;
	}

				  // FIXME: Implement me!
	return QVariant();
}

QHash<int, QByteArray> ListModel::roleNames() const
{
	QHash<int, QByteArray> rn = QAbstractListModel::roleNames();
	qDebug() << "Role Names: \n" << rn;
	rn.insert (DisplayRole, "DisplayRole");
	rn.insert (ImageIdRole, "ImageIdRole");
	rn.insert (ImageNameRole, "ImageNameRole");
	rn.insert (ImageDescriptionRole, "ImageDescriptionRole");
	return rn;
}

QString ListModel::getImageProviderRoot() const { return imageProviderRoot; }
