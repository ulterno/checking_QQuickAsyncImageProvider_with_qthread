#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QQmlContext>
#include "MainController.h"
#include "ImageProvider.h"

int main(int argc, char *argv[])
{
	QGuiApplication app (argc, argv);

	MainController *mc = new MainController (nullptr);
	ImageProvider *ip = new ImageProvider;

	QQmlApplicationEngine engine;

	engine.rootContext()->setContextProperty ("XListModel", mc->getLm());
	engine.addImageProvider (mc->getLm()->getImageProviderRoot(), ip);

	const QUrl url (u"qrc:/loi1/Main.qml"_qs);
	QObject::connect(&engine, &QQmlApplicationEngine::objectCreationFailed,
		&app, []() { QCoreApplication::exit(-1); },
		Qt::QueuedConnection);
	engine.load(url);

	return app.exec();
}
