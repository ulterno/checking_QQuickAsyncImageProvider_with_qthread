import QtQuick
import QtQuick.Window

Window {
	width: 640
	height: 480
	visible: true
	title: qsTr("Hello World")
	ListView {
		id: asdklk
		boundsBehavior: Flickable.StopAtBounds
		anchors.fill: parent
		model: XListModel
		delegate: Rectangle {
			border.color: "Black"
			implicitWidth: 300
			implicitHeight: 200
			Text {
				id: lVDelegateText
				width: 100
				text: DisplayRole
				color: "Black"
			}
			Image {
				width: 300
				height: 200
				id: lVDelegateImage
				source: ImageIdRole
			}
		}
	}
}
