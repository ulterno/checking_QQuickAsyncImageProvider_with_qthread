#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include <QObject>

#include "ListModel.h"

class MainController : public QObject
{
	Q_OBJECT
public:
	explicit MainController(QObject *parent = nullptr);

	ListModel *getLm () const;

signals:

private:
	ListModel *lm;
};

#endif // MAINCONTROLLER_H
