#include "ImageProvider.h"

#include <QDir>
#include <QFile>
#include <QStandardPaths>

ImageProvider::ImageProvider (ImageType type, Flags flags) : QQuickAsyncImageProvider() {}

QImage ImageProvider::requestImage (const QString &id, QSize *size, const QSize &requestedSize)
{
	qDebug() << "Image Requested :: " << id;
	QStringList slp = QStandardPaths::standardLocations (QStandardPaths::PicturesLocation);
	if (slp.isEmpty())
	{
		return QImage{};
	}
	QStringList ImagesList = QDir (slp.at (0) + "/Wallpapers/").entryList (QDir::NoDotAndDotDot | QDir::Files);
	qDebug() << "Image Files in " << slp.at (0) + "/Wallpapers/"
			 << "List :" << ImagesList;
	QImage aImage;
	if (!aImage.load (slp.at (0) + "/Wallpapers/" + ImagesList.at (id.toInt())))
	{
		qDebug() << "Error getting Image";
	}
	return aImage;
}
